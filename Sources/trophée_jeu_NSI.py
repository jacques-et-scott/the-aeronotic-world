import pygame
import random

# Définition des variables
largeur = 800
hauteur = 600
score = 0
point = 0
best_score = 0
FONT_SIZE = 30
game_started = False
game_over = False
game_paused = False
is_white=0
white_timer=0
best_score_color= (173,216,230)


# Initialisation du jeu
pygame.mixer.init()
victory_sound = pygame.mixer.Sound('son1.wav')
game_over_sound = pygame.mixer.Sound('son2.wav')
game_over_sound.set_volume(0.5)  # Ajuste le volume du son
pygame.init()
font = pygame.font.Font(None, 36)
fenetre = pygame.display.set_mode((largeur, hauteur))
pygame.display.set_caption("Jeu d'avion")
victory_font = pygame.font.Font(None, FONT_SIZE)
screen = pygame.display.set_mode((800, 600))

# Couleurs
bleu_clair = (173, 216, 230)
bleu_fonce = (25, 25, 112)
RED = (255, 0, 0)
yellow = (228, 223, 5)
white = (255, 255, 255)

# Chargement des images
avion = pygame.image.load('avion4.png')
avion = pygame.transform.scale(avion, (75, 75))
astéroide = pygame.image.load('asteroide.png')
astéroide = pygame.transform.scale(astéroide, (50, 50))

# Position initiale de l'avion
avion_x = 375
avion_y = 525

# Liste des astéroides
astéroides = []
astéroide_timer = 0

def play_victory_sound():
    victory_sound.play()

def play_game_over_sound():
    game_over_sound.play()

def rejouer():
    # Code pour recommencer le jeu
    print("Le jeu recommence")
    global game_over, score, point, astéroides
    game_over = False
    score = 0
    point = 0
    best_score = 5
    astéroides = []

# Affichage du menu de pause
def pause_menu():
    global game_paused
    while game_paused:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_c:  # Continuer
                    game_paused = False
                elif event.key == pygame.K_r:  # Recommencer
                    rejouer()
                    game_paused = False
                elif event.key == pygame.K_q:  # Quitter
                    pygame.quit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                x, y = event.pos
                if bouton_continue.collidepoint(x, y):  # Continuer
                    game_paused = False
                elif bouton_restart.collidepoint(x, y):  # Recommencer
                    rejouer()
                    game_paused = False
                elif bouton_quit.collidepoint(x, y):  # Quitter
                    pygame.quit()

        # Affichage des options de pause
        bouton_continue = pygame.Rect(300, 200, 200, 50)
        bouton_restart = pygame.Rect(300, 300, 200, 50)
        bouton_quit = pygame.Rect(300, 400, 200, 50)

        pygame.draw.rect(fenetre, bleu_fonce, bouton_continue)
        pygame.draw.rect(fenetre, bleu_fonce, bouton_restart)
        pygame.draw.rect(fenetre, bleu_fonce, bouton_quit)

        font = pygame.font.Font(None, 36)
        texte_continue = font.render("Continuer", True, white)
        texte_restart = font.render("Recommencer", True, white)
        texte_quit = font.render("Quitter", True, white)

        # Centrage du texte par rapport au bouton
        texte_continue_rect = texte_continue.get_rect(center=bouton_continue.center)
        texte_restart_rect = texte_restart.get_rect(center=bouton_restart.center)
        texte_quit_rect = texte_quit.get_rect(center=bouton_quit.center)

        fenetre.blit(texte_continue, texte_continue_rect)
        fenetre.blit(texte_restart, texte_restart_rect)
        fenetre.blit(texte_quit, texte_quit_rect)

        pygame.display.flip()


# Boucle principale du jeu 1
while not game_started:
    fenetre.fill((190, 160, 250))  # Remplit la fenêtre avec une couleur
    font = pygame.font.Font(None, 40)
    text = font.render("APPUYER SUR ESPACE POUR COMMENCER A JOUER", True, white)
    fenetre.blit(text, (largeur // 2 - text.get_width() // 2, hauteur // 2 - text.get_height() // 2))

    pygame.display.flip()

    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
            game_started = True


# Création d'une liste pour stocker les coordonnées des étoiles
etoiles = []
for _ in range(100):
    x = random.randint(0, largeur)
    y = random.randint(0, hauteur)
    etoiles.append((x, y))

# Boucle principale du jeu
running = True
while running:
    # Affichage des étoiles en arrière-plan
    fenetre.fill((16, 16, 60))  # Remplit la fenêtre avec une couleur
    for etoile in etoiles:
        pygame.draw.circle(fenetre, (255, 255, 255), etoile, 2)  # Dessine une étoile blanche de taille

    # Gestion des événements
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            x, y = event.pos
            if bouton_rect.collidepoint(x, y):
                rejouer()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:  # Appui sur la touche "Échap" pour mettre en pause
                game_paused = True

    # Si le jeu est en pause, afficher le menu de pause
    if game_paused:
        pause_menu()
    else:
        # Déplacement de l'avion
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            avion_x -= 1/2
            # Vérifier si l'avion ne sort pas par la gauche de la fenêtre
            if avion_x < 0:
                avion_x = 0
        if keys[pygame.K_RIGHT]:
            avion_x += 1/2
            # Vérifier si l'avion ne sort pas par la droite de la fenêtre
            if avion_x > largeur - 75:  # 75 représente la largeur de l'avion
                avion_x = largeur - 75

        # Création d'un nouveau astéroide
        astéroide_timer += 1/4
        if astéroides == [] or astéroide_timer % 100 == 0:
            astéroides.append([random.randint(0, 750), 0])

        # Déplacement et affichage des astéroides
        for astéroide_pos in astéroides:
            astéroide_pos[1] += 1/6
            fenetre.blit(astéroide, (astéroide_pos[0], astéroide_pos[1]))
            if score >= 5:
                astéroide_pos[1] += 1/16 + 1/128
            if score >= 20:
                astéroide_pos[1] += 1/16 + 2/128
            if score >= 40:
                astéroide_pos[1] += 1/16 + 3/128
            if score >= 60:
                astéroide_pos[1] += 1/16 + 4/128
            if score >= 100:
                astéroide_pos[1] += 1/16 + 7/128
            if score >= 140:
                astéroide_pos[1] += 1/16 + 8/128
            if score >= 180:
                astéroide_pos[1] += 1/16 + 10/128
            if score >= 220:
                astéroide_pos[1] += 1/16 + 12/128
                astéroide_timer += 1/4 + 1
            if score >= 260:
                astéroide_pos[1] += 1/16 + 14/128
            if score >= 300:
                astéroide_pos[1] += 1/16 + 16/128
            if score >= 400:
                astéroide_timer += 1/4 + 2
                astéroide_pos[1] += 1/16 + 18/128
            if score >= 500:
                astéroide_pos[1] += 1/16 + 26/128
            if score >= 750:
                astéroide_pos[1] += 1/16 + 34/128
                astéroide_timer += 1/4 + 3

            if astéroide_pos[1] > 600:
                astéroides.remove(astéroide_pos)
                score += 1

        game_over_sound.set_volume(0.5)  # Ajuste le volume du son

        # Affichage de l'avion
        fenetre.blit(avion, (avion_x, avion_y))

        # Affichage des Astéroides Touchés
        text = font.render("Astéroides Touchés : " + str(point), True, RED)
        fenetre.blit(text, (largeur // 2 - text.get_width() // 2, 50))

        best_score_text = victory_font.render("Meilleur Score : " + str(best_score), True, white)
        fenetre.blit(best_score_text, (10, 10))

        # Affichage du Temps de la partie
        text = font.render("SCORE : " + str(score), True, yellow)
        fenetre.blit(text, (largeur // 2 - text.get_width() // 2, 10))

        # Vérification de collision avec les astéroides
        for astéroide_pos in astéroides:
            if abs(avion_x - astéroide_pos[0]) < 50 and abs(avion_y - astéroide_pos[1]) < 50:
                astéroides.remove(astéroide_pos)
                point += 1

                # Condition de Victoire
        if score == 1000:
            font = pygame.font.SysFont(None, 90)
            text_victory = font.render("Victory", True, white,)
            screen.blit(text_victory, (largeur // 2 - text_victory.get_width() // 2, hauteur // 2 - text_victory.get_height() // 2))
            pygame.time.wait(100)
            for _ in range (1):
                play_victory_sound()


        # Condition de défaite
        if point == 10:
            game_over = True
            font = pygame.font.SysFont(None, 160)
            text = font.render("Game Over", True, RED)
            screen.blit(text, (largeur // 2 - text.get_width() // 2, hauteur // 2 - text.get_height() // 2))
            pygame.time.wait(100)
            for _ in range (1):
                play_game_over_sound()


        # Affichage du meilleur score
        if score > best_score:
            best_score = score
            is_white = True
            white_timer = 0

        if score < best_score:
            is_white = False
            white_timer = 0

        if is_white:
            white_timer += 1
            if white_timer % 300 == 0:  # Change la couleur toutes les 300 frames
                is_white = False
                best_score_color = bleu_fonce
        else:
            white_timer += 6
            if white_timer % 300 == 0:  # Change la couleur toutes les 300 frames
                is_white = True
                best_score_color = white

        best_score_text = victory_font.render("Meilleur Score : " + str(best_score), True, best_score_color)
        fenetre.blit(best_score_text, (10, 10))

        # Affichage du bouton "Rejouer" si le jeu est terminé
        if game_over:
            bouton_rect = pygame.Rect(300, 500, 200, 50)
            pygame.draw.rect(fenetre, bleu_fonce, bouton_rect)
            font = pygame.font.Font(None, 36)
            texte = font.render(" Rejouer", True, white)
            fenetre.blit(texte, (340, 510))

        pygame.display.flip()

pygame.quit()
